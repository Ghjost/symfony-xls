# Symfony XLSX <!-- omit in toc -->

## Table of content <!-- omit in toc -->

- [Project](#project)
- [Todo list](#todo-list)
- [Installation](#installation)
- [Technos](#technos)
- [Required keys (API)](#required-keys-api)
- [Screen captures](#screen-captures)
  - [Export from form](#export-from-form)
  - [Import to form](#import-to-form)
  - [XLSX file content](#xlsx-file-content)

---

## Project

This example project have to goal to import and export XLSX files to/from form with [PhpSpreadsheet](https://phpspreadsheet.readthedocs.io/en/latest/) bundle.

---

## Todo list

- [ ] Create home page
- [x] Export XLSX from form
- [ ] Export custom XLSX from table
- [x] Import XLSX to form
- [ ] Import custom XLSX to table
- [ ] Set dockerfile to load project from Docker

---

## Installation

After pulling project install dependencies by run

    composer install

And that's all !

---

## Technos

- PHP (Thanks captain obvious)
- Symfony
- Twig (included with Symfony for this project)
- Jquery
- Bootstrap
- Font Awesome

---

## Required keys (API)

This project required part of location, I choose to use [Position Stack API](https://positionstack.com/) for geocoding (Free version is pretty ok for this kind of project).

After create an account, change API key in `.env file`
![.env file api key change](https://i.imgur.com/pO1GxV2.png)

---

## Screen captures

### Export from form

![Export construction site from form to XLSX file](https://i.imgur.com/mVGE0YO.png)
![Successfully export construction site from form to XLSX file](https://i.imgur.com/dMKqQ8I.png)

### Import to form

![Import contruction site file (XLSX) to form](https://i.imgur.com/xFNskmJ.png)
![Successfully import contruction site file (XLSX) to form](https://i.imgur.com/7VY9I5g.png)

### XLSX file content

![Construction site worksheet page](https://i.imgur.com/S5rVSwM.png)
![Construction site worksheet page](https://i.imgur.com/vJR4kfJ.png)
