<?php

namespace App\Controller;

use App\Entity\ConstructionSite;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Uid\Ulid;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ImportXlsController extends AbstractController
{
    #[Route('/import/xls', name:'import_xls')]
function index(): Response
    {
    return $this->render('import_xls/index.html.twig', [
        'controller_name' => 'ImportXlsController',
    ]);
}

#[Route('/import/xls/submit', name:'import_xls_submit')]
function submit(
    Request $request,
    HttpClientInterface $client,
    NormalizerInterface $normalizerInterface,
    SluggerInterface $slugger
): Response {

    // Define file
    $importedFile = $request->files->get('file');

    $fileType = strtolower(pathinfo($importedFile->getClientOriginalName(), PATHINFO_EXTENSION));
    $fileName = strtolower(pathinfo($importedFile->getClientOriginalName(), PATHINFO_FILENAME));
    $safeFilename = $slugger->slug($fileName) . '-' . uniqid() . '.' . $fileType;

    // $importedFile->move($importedFile, './temp/upload/' . $importedFile->getClientOriginalName());
    move_uploaded_file($importedFile, './temp/upload/' . $safeFilename);

    $cs = new ConstructionSite();
    $cs->setPublicId(strval(new Ulid()));
    $cs->setFilename('./temp/upload/' . $safeFilename);

    // Create reader and spreasheet
    $reader = new Xlsx();
    $spreadSheet = $reader->load($cs->getFilename());

    // Get all worksheets names
    $worksheetNames = array();
    foreach ($reader->listWorksheetNames($cs->getFilename()) as $key => $worksheetName) {
        $worksheetNames[$key] = $worksheetName;
    }

    if ($worksheetNames[0] == 'construction_site') {
        $constructionSite = $spreadSheet->getSheetByName($worksheetNames[0])->toArray();

        $constructionSiteDetails = array();
        if ($constructionSite[1][0] == null || $constructionSite[1][0] == 'LAISSER VIDE SI NUL') {
            $constructionSiteDetails['publicId'] = strval(new Ulid());
        } else {
            $constructionSiteDetails['publicId'] = $constructionSite[1][0];
        }
        $constructionSiteDetails['name'] = $constructionSite[1][1];
        $constructionSiteDetails['start_at'] = $constructionSite[1][5];
        $constructionSiteDetails['end_at'] = $constructionSite[2][5];
        $constructionSiteDetails['location']['country'] = $constructionSite[1][3];
        $constructionSiteDetails['location']['region'] = $constructionSite[2][3];
        $constructionSiteDetails['location']['city'] = $constructionSite[3][3];
        $constructionSiteDetails['location']['address'] = $constructionSite[4][3];
        $constructionSiteDetails['location']['coord_lat'] = $constructionSite[5][3];
        $constructionSiteDetails['location']['coord_long'] = $constructionSite[6][3];
    } else {
        dd('error, first worksheet is not construction_site');
    }

    if ($worksheetNames[1] == 'cs_operations') {
        $constructionSiteOperations = $spreadSheet->getSheetByName($worksheetNames[1])->toArray();
        foreach ($constructionSiteOperations[0] as $key => $operation) {
            if ($key != 0) {
                $constructionSiteDetails['operation'][$key]['name'] = $operation;
                $constructionSiteDetails['operation'][$key]['details'] = $constructionSiteOperations[1][$key];
                $constructionSiteDetails['operation'][$key]['start_at'] = $constructionSiteOperations[2][$key];
                $constructionSiteDetails['operation'][$key]['end_at'] = $constructionSiteOperations[3][$key];
            }
        }

    } else {
        dd('error, second worksheet is not cs_operations');
    }

    // dd($spreadSheet, $worksheetNames, $constructionSite, $constructionSiteDetails, $constructionSiteOperations);

    dump($constructionSiteDetails);

    unlink('./temp/upload/' . $safeFilename);

    return $this->render('import_xls/index.html.twig', [
        'controller_name' => 'ExportXlsController',
        'construction_site_details' => $constructionSiteDetails,
    ]);
}
}
