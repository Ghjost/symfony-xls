<?php

namespace App\Controller;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Uid\Ulid;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ExportXlsController extends AbstractController
{
    #[Route('/export/xls', name:'export_xls')]
function index(
): Response {

    return $this->render('export_xls/index.html.twig', [
        'controller_name' => 'ExportXlsController',
    ]);
}

#[Route('/geocode', name:'geocode', methods:['POST'])]
function geoCode(
    Request $request,
    HttpClientInterface $client
): Response {

    return $this->render('export_xls/index.html.twig', [
        'controller_name' => 'ExportXlsController',
        'file' => 'file',
    ]);
}

#[Route('/export/xls/submit', name:'export_xls_submit')]
function submit(
    Request $request,
    HttpClientInterface $client,
    NormalizerInterface $normalizerInterface,
    SluggerInterface $sluggerInterface
): Response {
    $requestContent = $request->request->all();

    $csInfo['construction_site']['name'] = $requestContent['cs_name'];
    $csInfo['construction_site']['start_at'] = $requestContent['cs_start_at'];
    $csInfo['construction_site']['end_at'] = $requestContent['cs_end_at'];

    $operationList = array();
    foreach ($requestContent['operation_name'] as $key => $value) {
        $operationList['operations'][$key]['name'] = $requestContent['operation_name'][$key];
        $operationList['operations'][$key]['detail'] = $requestContent['operation_detail'][$key];
        $operationList['operations'][$key]['start_at'] = $requestContent['operation_start_at'][$key];
        $operationList['operations'][$key]['end_at'] = $requestContent['operation_end_at'][$key];
    }

    // dd($operationList, $requestContent['operation_name'], $requestContent);

    $adressResponse = $client->request('GET', 'http://api.positionstack.com/v1/forward?access_key=' . urlencode($this->getParameter('api.positionstack')) . '&query=' . $requestContent['cs_address'])->toArray()['data'];

    $adressNormalized = $normalizerInterface->normalize($adressResponse, null, []);

    $adress['country'] = $adressNormalized[0]['country'];
    $adress['region'] = $adressNormalized[0]['region'];
    $adress['city'] = $adressNormalized[0]['locality'];
    $adress['adress'] = $adressNormalized[0]['name'];
    $adress['coord_lat'] = $adressNormalized[0]['latitude'];
    $adress['coord_long'] = $adressNormalized[0]['longitude'];

    $styleTitle = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => Alignment::HORIZONTAL_CENTER,
        ],
    ];

    $spreadSheet = new Spreadsheet();

    $csSheet = new Worksheet(null, 'construction_site');
    $csSheet->getDefaultColumnDimension()->setWidth(24);

    $csSheet->setCellValue('A1', 'PUBLICID DU CHANTIER');
    $csSheet->setCellValue('A2', strval(new Ulid()));

    $csSheet->setCellValue('B1', 'NOM DU CHANTIER*');
    $csSheet->setCellValue('B2', $csInfo['construction_site']['name']);

    $csSheet->mergeCells('C1:D1');
    $csSheet->setCellValue('C1', 'ADRESSE DU CHANTIER*');
    $csSheet->setCellValue('C2', 'PAYS');
    $csSheet->setCellValue('D2', $adress['country']);
    $csSheet->setCellValue('C3', 'REGION');
    $csSheet->setCellValue('D3', $adress['region']);
    $csSheet->setCellValue('C4', 'VILLE');
    $csSheet->setCellValue('D4', $adress['city']);
    $csSheet->setCellValue('C5', 'ADRESSE');
    $csSheet->setCellValue('D5', $adress['adress']);
    $csSheet->setCellValue('C6', 'COORD LAT*');
    $csSheet->setCellValue('D6', $adress['coord_lat']);
    $csSheet->setCellValue('C7', 'COORD LONG*');
    $csSheet->setCellValue('D7', $adress['coord_long']);

    $csSheet->mergeCells('E1:F1');
    $csSheet->setCellValue('E1', 'DATES DU CHANTIER*');
    $csSheet->setCellValue('E2', 'DATE DE DEBUT*');
    $csSheet->setCellValue('F2', $csInfo['construction_site']['start_at']);
    $csSheet->setCellValue('E3', 'DATE DE FIN*');
    $csSheet->setCellValue('F3', $csInfo['construction_site']['end_at']);

    $spreadSheet->addSheet($csSheet, 0);

    $operationArray = [
        $requestContent['operation_name'],
        $requestContent['operation_detail'],
        $requestContent['operation_start_at'],
        $requestContent['operation_end_at'],
    ];

    $csOperarations = new Worksheet(null, 'cs_operations');
    $csOperarations->getDefaultColumnDimension()->setWidth(38);

    $csOperarations->fromArray($operationArray, null, 'B1');
    $csOperarations->setCellValue('A1', 'NOM DE L\'OPERATION');
    $csOperarations->setCellValue('A2', 'DETAIL DE L\'OPERATION');
    $csOperarations->setCellValue('A3', 'DATE DE DEBUT DE L\'OPERATION');
    $csOperarations->setCellValue('A4', 'DATE DE FIN DE L\'OPERATION');

    $spreadSheet->addSheet($csOperarations, 1);

    $sheetIndex = $spreadSheet->getIndex(
        $spreadSheet->getSheetByName('Worksheet')
    );
    $spreadSheet->removeSheetByIndex($sheetIndex);
    $spreadSheet->setActiveSheetIndex(0);

    $XlsxWriter = new Xlsx($spreadSheet);
    $XlsxWriter->save($sluggerInterface->slug($csInfo['construction_site']['name']) . '.xlsx');

    return $this->render('export_xls/index.html.twig', [
        'controller_name' => 'ExportXlsController',
        'file' => '/' . $sluggerInterface->slug($csInfo['construction_site']['name']) . '.xlsx',
    ]);
}
}
